# net-cfg-parser

#### 介绍
基于antlr4设计网络配置文件DSL，通过语法解释器解析网络配置，提取网络配置信息

#### 软件架构
软件架构说明


#### 编译

```
$ cd ./net-cfg-parser
$ ./build.sh
```

#### 使用说明

启动服务
```
$ ./bin/control start
```

关闭服务
```
$ ./bin/control stop
```

获取指定设备的配置文件解析结构化结果（配置文件默认存放到当前工作目录./backups子目录下）
```
$ curl -XGET 'http://localhost:8870/v1/config/file?mgmtIp=10.131.21.22'
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
