package org.movee.net.cfg.parser.api.doc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@EnableOpenApi
public class Knife4jConfig {

    @Bean(value = "oasConfig")
    public Docket oasConfig() {
        Docket docket=new Docket(DocumentationType.OAS_30)
                .apiInfo(new ApiInfoBuilder()
                        .title("network-configuration-parser")
                        .description("网络设备配置文件解析")
                        .version("v1")
                        .build())
                .groupName("nconf-parser-v1")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.nconf.parser.api.v1"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

}
