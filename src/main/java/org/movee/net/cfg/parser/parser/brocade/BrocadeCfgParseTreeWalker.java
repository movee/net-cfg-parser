package org.movee.net.cfg.parser.parser.brocade;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.domain.parser.IndentedRowConfigNode;
import org.movee.net.cfg.parser.parser.h3c.H3cConfigEntityType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

import static org.movee.net.cfg.parser.parser.brocade.BrocadeConfigEntityType.INTERFACE;

/**
 *
 *
 * @author 
 */
@Slf4j
public class BrocadeCfgParseTreeWalker {

    private BrocadeConfigNode currCfgNode = null;
    private final BrocadeConfigContext context;
    private final BrocadeSystemParser systemParser = new BrocadeSystemParser();
    private final BrocadeIntfParser intfParser = new BrocadeIntfParser();
    private final BrocadeIntfIpParser ipParser = new BrocadeIntfIpParser();
    private final BrocadeIntfVlanParser vlanParser = new BrocadeIntfVlanParser();
    private final ObjectMapper jacksonMapper = new ObjectMapper();

    public BrocadeCfgParseTreeWalker(String mgmtIp, String vendor) {
        this.context = new BrocadeConfigContext(mgmtIp, vendor);
        this.currCfgNode = createRootConfigNode(context);
    }

    public BrocadeConfigModel getModel() {
        return context.getModel();
    }

    public void walk(IndentedRowConfigNode node) {

        enterFile(node);

        List<IndentedRowConfigNode> children = node.getChildren();
        if (children != null) {
            for (IndentedRowConfigNode child : children) {
                if (!pruneRoot(child)) {
                    internalWalk(child);
                }
            }
        }

        exitFile(node);
    }

    /**
     * 深度优先遍历AST tree
     * @param node tree node
     */
    private void internalWalk(IndentedRowConfigNode node) {
        enterRow(node);

        List<IndentedRowConfigNode> children = node.getChildren();
        if (children != null) {
            for (IndentedRowConfigNode child : children) {
                internalWalk(child);
            }
        }

        exitRow(node);
    }

    public void enterFile(IndentedRowConfigNode node) {

    }

    public void exitFile(IndentedRowConfigNode node) {

    }

    public void enterRow(IndentedRowConfigNode node) {

        IndentedRowCfgParser.RowContext ctx = node.getCtx();

        BrocadeConfigNode cfgNode = createConfigNode(currCfgNode, ctx);
        switch (cfgNode.getEntityType()) {
            case SYSTEM: {
                systemParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE: {
                log.info("i: {}", ctx.getText());
                intfParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE_IP: {
                ipParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE_VLAN: {
                vlanParser.parse(cfgNode, ctx);
            }
            default: {

            }
        }

        currCfgNode = cfgNode;

    }

    public void exitRow(IndentedRowConfigNode node) {
        if (currCfgNode.getIsEntityRoot()) {
            BrocadeConfigContext context = currCfgNode.getContext();
            BrocadeConfigModel model = context.getModel();
            BrocadeConfigEntityType type = currCfgNode.getEntityType();

            Object entityInstance = context.remParsingEntity(type);
            switch (type) {
                case INTERFACE: {
                    BrocadeConfigModel.Interface intf = (BrocadeConfigModel.Interface) entityInstance;
                    model.putIntf(intf.getName(), intf);
                    break;
                }
                case INTERFACE_IP: {
                    BrocadeConfigModel.Interface intf = (BrocadeConfigModel.Interface) context.getParsingEntity(INTERFACE);
                    intf.getIps().add((BrocadeConfigModel.InterfaceIp) entityInstance);
                    break;
                }
                case INTERFACE_VLAN: {
                    BrocadeConfigModel.Interface intf = (BrocadeConfigModel.Interface) context.getParsingEntity(INTERFACE);
                    intf.getVlans().add((BrocadeConfigModel.InterfaceVlan) entityInstance);
                    break;
                }
            }
        }

        currCfgNode = currCfgNode.getParent();
    }

    private boolean pruneRoot(IndentedRowConfigNode node) {
        IndentedRowCfgParser.RowContext ctx = node.getCtx();

        TerminalNode first = ctx.WORD(0);
        // 不是interface
        return first == null || !H3cConfigEntityType.INTERFACE.getKey().equals(first.getText());
    }


    private BrocadeConfigNode createConfigNode(BrocadeConfigNode parent, IndentedRowCfgParser.RowContext ctx) {

        String word0 = ctx.WORD(0) == null ? "" : ctx.WORD(0).getText();
        String word1 = ctx.WORD(1) == null ? "" : ctx.WORD(1).getText();
        String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();

        BrocadeConfigNode cfgNode = new BrocadeConfigNode();
        cfgNode.setParent(parent);
        cfgNode.setWord0(word0);
        cfgNode.setWord1(word1);
        cfgNode.setIsRoot(false);
        cfgNode.setContext(parent.getContext());

        if ("switch-attributes".equals(word0) && parent.getIsRoot()) {
            log.info("{}", ctx.getText());
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(BrocadeConfigEntityType.SYSTEM);
        } else if ("interface".equals(word0) && parent.getIsRoot()) {
            log.info("{}", ctx.getText());
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(BrocadeConfigEntityType.INTERFACE);
        } else if ("ip".equals(word0) && "address".equals(word1)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == BrocadeConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(BrocadeConfigEntityType.INTERFACE_IP);
        } else if ("switchport".equals(word0) && "access".equals(word1) && "vlan".equals(word2)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == BrocadeConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(BrocadeConfigEntityType.INTERFACE_VLAN);
        } else if (parent.getIsRoot()) {
            // 暂不支持解析的一级节点
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(BrocadeConfigEntityType.UNKNOWN);
        } else {
            // 支持解析或不支持解析的非根节点
            cfgNode.setIsEntityRoot(false);
            // 继承父节点的
            cfgNode.setEntityType(parent.getEntityType());
        }

        return cfgNode;
    }


    private BrocadeConfigNode createRootConfigNode(BrocadeConfigContext context) {
        BrocadeConfigNode cfgNode = new BrocadeConfigNode();
        cfgNode.setParent(null);
        cfgNode.setWord0("root");
        cfgNode.setWord1("root");
        cfgNode.setIsRoot(true);
        cfgNode.setIsEntityRoot(false);
        cfgNode.setEntityType(BrocadeConfigEntityType.ROOT);
        cfgNode.setContext(context);
        return cfgNode;
    }


}
