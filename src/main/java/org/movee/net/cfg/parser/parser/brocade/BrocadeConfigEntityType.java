package org.movee.net.cfg.parser.parser.brocade;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 *
 * @author 
 */
@Getter
@AllArgsConstructor
public enum BrocadeConfigEntityType {

    ROOT(""),
    SYSTEM("system"),
    INTERFACE("interface"),
    INTERFACE_IP("ip"),
    INTERFACE_VLAN("vlan"),

    UNKNOWN("unknown");

    private final String key;

}
