package org.movee.net.cfg.parser.parser.brocade;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

import static org.movee.net.cfg.parser.parser.brocade.BrocadeConfigEntityType.INTERFACE;
import static org.movee.net.cfg.parser.parser.brocade.BrocadeConfigModel.Interface;

/**
 *
 *
 * @author 
 */
public class BrocadeIntfParser {

    public void parse(BrocadeConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        BrocadeConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();
        String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();

        if (configNode.getIsEntityRoot()) {
            Interface intf = new Interface().setName(word1 + " " + word2);

            context.setParsingEntity(INTERFACE, intf);
        } else {
            Interface intf = (Interface) context.getParsingEntity(INTERFACE);

            BrocadeConfigNode parent = configNode.getParent();
            if ("description".equals(word0) && parent.getIsEntityRoot()) {
                intf.setDescription(word1);
            }
        }

    }
}
