package org.movee.net.cfg.parser.parser.xorplus;

/**
 *
 *
 * @author 
 */
public class XorplusSystemParser {

    public void parse(XorplusConfigNode configNode) {
        XorplusConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        XorplusConfigNode parent = configNode.getParent();
        if (parent.getIsEntityRoot()) {
            XorplusConfigModel.Netware netware = context.getModel().getNetware();
            if ("hostname".equals(word0)) {
                netware.setHostname(word1);
            }

        }
    }

}
