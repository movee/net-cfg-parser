package org.movee.net.cfg.parser.parser.huawei;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.domain.model.InterfaceStatus;
import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigModel.Interface;

/**
 *
 *
 * @author 
 */
public class HuaweiIntfParser {

    public void parse(HuaweiConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        HuaweiConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            Interface intf = new Interface().setName(word1).setAdminStatus(InterfaceStatus.UP.name());

            context.setParsingEntity(HuaweiConfigEntityType.INTERFACE, intf);
        } else {
            Interface intf = (Interface) context.getParsingEntity(HuaweiConfigEntityType.INTERFACE);

            HuaweiConfigNode parent = configNode.getParent();
            if ("description".equals(word0) && parent.getIsEntityRoot()) {
                intf.setDescription(word1);
            }
            if ("shutdown".equals(word0) && parent.getIsEntityRoot()) {
                intf.setAdminStatus(InterfaceStatus.DOWN.name());
            }
            if ("mtu".equals(word0) && parent.getIsEntityRoot()) {
                intf.setMtu(ParserUtils.parseNullableLong(word1));
            }
        }

    }
}
