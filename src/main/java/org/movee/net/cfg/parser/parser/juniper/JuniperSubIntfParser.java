package org.movee.net.cfg.parser.parser.juniper;


import static org.movee.net.cfg.parser.parser.juniper.JuniperConfigEntityType.SUB_INTERFACE;


/**
 *
 *
 * @author 
 */
public class JuniperSubIntfParser {

    public void parse(JuniperConfigNode configNode) {
        JuniperConfigContext context = configNode.getContext();
        String argument = configNode.getWord1();

        JuniperConfigNode parent = configNode.getParent();
        if (configNode.getIsEntityRoot()) {
            String name = parent.getWord0() + "." + argument;
            JuniperConfigModel.Interface intf = new JuniperConfigModel.Interface().setName(name);

            context.setParsingEntity(SUB_INTERFACE, intf);
        }
    }

}
