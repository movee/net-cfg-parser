package org.movee.net.cfg.parser.parser.h3c;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

/**
 *
 *
 * @author 
 */
public class H3cSystemParser {

    public void parse(H3cConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        H3cConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        H3cConfigNode parent = configNode.getParent();
        if (parent.getIsRoot()) {
            H3cConfigModel.Netware netware = context.getModel().getNetware();
            if ("sysname".equals(word0)) {
                netware.setHostname(word1);
            }

            if ("version".equals(word0)) {
                String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
                String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();

                netware.setSoftwareVersion(String.join(" ", word1, word2, word3));
            }
        }

    }
}
