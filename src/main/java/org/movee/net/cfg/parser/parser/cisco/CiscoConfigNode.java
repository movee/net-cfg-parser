package org.movee.net.cfg.parser.parser.cisco;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CiscoConfigNode {

    // 父节点
    private CiscoConfigNode parent;

    private String word0;
    private String word1;

    private Boolean isRoot = false;
    // 是某个entity的根node
    private Boolean isEntityRoot = false;

    // 当前解析节点所属的entity type
    private CiscoConfigEntityType entityType;

    private CiscoConfigContext context;

}
