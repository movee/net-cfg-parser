package org.movee.net.cfg.parser.parser.brocade;

import lombok.Data;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Data
public class BrocadeConfigContext {

    private String mgmtIp;
    private String vendor;

    // 解析出来的配置模型
    private BrocadeConfigModel model;

    // 当前正在解析的 ConfigModelEntity,  type -> ConfigModelEntity
    private Map<BrocadeConfigEntityType, Object> parsingEntities;

    public BrocadeConfigContext(String mgmtIp, String vendor) {
        this.mgmtIp = mgmtIp;
        this.vendor = vendor;
        this.model = new BrocadeConfigModel();
        this.model.setMgmtIp(mgmtIp);
        this.model.setTimestamp(Instant.now().toEpochMilli());
        this.model.getNetware().setMgmtIp(mgmtIp).setVendor(vendor);
        this.parsingEntities = new LinkedHashMap<>();
    }

    public Object getParsingEntity(BrocadeConfigEntityType type) {
        return parsingEntities.get(type);
    }

    public void setParsingEntity(BrocadeConfigEntityType type, Object entity) {
        parsingEntities.put(type, entity);
    }

    public Object remParsingEntity(BrocadeConfigEntityType type) {
        return parsingEntities.remove(type);
    }


}
