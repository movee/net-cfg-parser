package org.movee.net.cfg.parser.parser.ruijie;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.domain.parser.IndentedRowConfigNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 *
 *
 * @author 
 */
@Slf4j
public class RuijieCfgParseTreeWalker {

    private RuijieConfigNode currCfgNode = null;
    private final RuijieConfigContext context;
    private final RuijieSystemParser sysParser = new RuijieSystemParser();
    private final RuijieIntfParser intfParser = new RuijieIntfParser();
    private final RuijieIntfIpParser ipParser = new RuijieIntfIpParser();
    private final RuijieIntfIp6Parser ip6Parser = new RuijieIntfIp6Parser();
    private final RuijieBgpParser bgpParser = new RuijieBgpParser();
    private final ObjectMapper jacksonMapper = new ObjectMapper();

    public RuijieCfgParseTreeWalker(String mgmtIp, String vendor) {
        this.context = new RuijieConfigContext(mgmtIp, vendor);
        this.currCfgNode = createRootConfigNode(context);
    }

    public RuijieConfigModel getModel() {
        return context.getModel();
    }

    public void walk(IndentedRowConfigNode node) {

        enterFile(node);

        List<IndentedRowConfigNode> children = node.getChildren();
        if (children != null) {
            for (IndentedRowConfigNode child : children) {
                internalWalk(child);
            }
        }

        exitFile(node);
    }

    /**
     * 深度优先遍历AST tree
     * @param node tree node
     */
    private void internalWalk(IndentedRowConfigNode node) {
        enterRow(node);

        List<IndentedRowConfigNode> children = node.getChildren();
        if (children != null) {
            for (IndentedRowConfigNode child : children) {
                internalWalk(child);
            }
        }

        exitRow(node);
    }

    public void enterFile(IndentedRowConfigNode node) {

    }

    public void exitFile(IndentedRowConfigNode node) {

    }

    public void enterRow(IndentedRowConfigNode node) {

        IndentedRowCfgParser.RowContext ctx = node.getCtx();

        RuijieConfigNode cfgNode = createConfigNode(currCfgNode, ctx);
        switch (cfgNode.getEntityType()) {
            case SYSTEM: {
                sysParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE: {
                intfParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE_IP: {
                ipParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE_IP6: {
                ip6Parser.parse(cfgNode, ctx);
                break;
            }
            case BGP: {
                bgpParser.parse(cfgNode, ctx);
                break;
            }
            default: {
                //
            }
        }

        currCfgNode = cfgNode;

    }

    public void exitRow(IndentedRowConfigNode node) {
        if (currCfgNode.getIsEntityRoot()) {
            RuijieConfigContext context = currCfgNode.getContext();
            RuijieConfigModel model = context.getModel();
            RuijieConfigEntityType type = currCfgNode.getEntityType();

            Object entityInstance = context.remParsingEntity(type);
            switch (type) {
                case INTERFACE: {
                    RuijieConfigModel.Interface intf = (RuijieConfigModel.Interface) entityInstance;
                    model.putIntf(intf.getName(), intf);
                    break;
                }
                case INTERFACE_IP: {
                    RuijieConfigModel.Interface intf = (RuijieConfigModel.Interface) context.getParsingEntity(RuijieConfigEntityType.INTERFACE);
                    intf.getIps().add((RuijieConfigModel.InterfaceIp) entityInstance);
                    break;
                }
                case INTERFACE_IP6: {
                    RuijieConfigModel.Interface intf = (RuijieConfigModel.Interface) context.getParsingEntity(RuijieConfigEntityType.INTERFACE);
                    intf.getIp6s().add((RuijieConfigModel.InterfaceIp6) entityInstance);
                    break;
                }
                case BGP: {
                    RuijieConfigModel.Bgp bgp = (RuijieConfigModel.Bgp) entityInstance;
                    model.setBgp(bgp);
                    break;
                }
                default: {
                    //
                }
            }
        }

        currCfgNode = currCfgNode.getParent();
    }

    private RuijieConfigNode createConfigNode(RuijieConfigNode parent, IndentedRowCfgParser.RowContext ctx) {

        String word0 = ctx.WORD(0) == null ? "" : ctx.WORD(0).getText();
        String word1 = ctx.WORD(1) == null ? "" : ctx.WORD(1).getText();

        RuijieConfigNode cfgNode = new RuijieConfigNode();
        cfgNode.setParent(parent);
        cfgNode.setWord0(word0);
        cfgNode.setWord1(word1);
        cfgNode.setIsRoot(false);
        cfgNode.setContext(parent.getContext());

        if ("hostname".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(RuijieConfigEntityType.SYSTEM);
        } else if ("version".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(RuijieConfigEntityType.SYSTEM);
        } else if ("interface".equals(word0) && parent.getIsRoot()) {
            log.debug("{}", ctx.getText());
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(RuijieConfigEntityType.INTERFACE);
        } else if ("bgp".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(RuijieConfigEntityType.BGP);
        } else if ("ip".equals(word0) && "address".equals(word1)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == RuijieConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(RuijieConfigEntityType.INTERFACE_IP);
        } else if ("ipv6".equals(word0) && "address".equals(word1)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == RuijieConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(RuijieConfigEntityType.INTERFACE_IP6);
        } else if (parent.getIsRoot()) {
            // 暂不支持解析的一级节点
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(RuijieConfigEntityType.UNKNOWN);
        } else {
            // 支持解析或不支持解析的非根节点
            cfgNode.setIsEntityRoot(false);
            // 继承父节点的
            cfgNode.setEntityType(parent.getEntityType());
        }

        return cfgNode;
    }


    private RuijieConfigNode createRootConfigNode(RuijieConfigContext context) {
        RuijieConfigNode cfgNode = new RuijieConfigNode();
        cfgNode.setParent(null);
        cfgNode.setWord0("root");
        cfgNode.setWord1("root");
        cfgNode.setIsRoot(true);
        cfgNode.setIsEntityRoot(false);
        cfgNode.setEntityType(RuijieConfigEntityType.ROOT);
        cfgNode.setContext(context);
        return cfgNode;
    }



}
