package org.movee.net.cfg.parser.parser.brocade;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

public class BrocadeSystemParser {

    public void parse(BrocadeConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {
        BrocadeConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        BrocadeConfigNode parent = configNode.getParent();
        if (parent.getIsRoot()) {
            BrocadeConfigModel.Netware netware = context.getModel().getNetware();
            if ("host-name".equals(word0)) {
                netware.setHostname(word1);
            }

        }
    }

}
