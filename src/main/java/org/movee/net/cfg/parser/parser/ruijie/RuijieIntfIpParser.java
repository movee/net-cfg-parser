package org.movee.net.cfg.parser.parser.ruijie;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

/**
 *
 *
 * @author 
 */
public class RuijieIntfIpParser {

    public void parse(RuijieConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        RuijieConfigContext context = configNode.getContext();
        String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
        String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();

        if (configNode.getIsEntityRoot()) {
            RuijieConfigModel.InterfaceIp ip = new RuijieConfigModel.InterfaceIp().setIpAddress(word2).setIpMask(word3);
            context.setParsingEntity(RuijieConfigEntityType.INTERFACE_IP, ip);
        }

    }

}
