package org.movee.net.cfg.parser.parser.huawei;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;


/**
 *
 *
 * @author 
 */
public class HuaweiSystemParser {

    public void parse(HuaweiConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        HuaweiConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        HuaweiConfigNode parent = configNode.getParent();
        if (parent.getIsRoot()) {
            HuaweiConfigModel.Netware netware = context.getModel().getNetware();
            if ("sysname".equals(word0)) {
                netware.setHostname(word1);
            }

            if ("!Software".equals(word0) && "Version".equals(word1)) {
                String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
                netware.setSoftwareVersion(word2);
            }
        }

    }

}
