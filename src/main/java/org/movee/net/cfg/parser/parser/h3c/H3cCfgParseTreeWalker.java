package org.movee.net.cfg.parser.parser.h3c;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.domain.parser.IndentedRowConfigNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static org.movee.net.cfg.parser.parser.h3c.H3cConfigModel.Bgp;
import static org.movee.net.cfg.parser.parser.h3c.H3cConfigModel.Interface;
import static org.movee.net.cfg.parser.parser.h3c.H3cConfigModel.InterfaceIp;
import static org.movee.net.cfg.parser.parser.h3c.H3cConfigModel.InterfaceIp6;
import static org.movee.net.cfg.parser.parser.h3c.H3cConfigEntityType.INTERFACE;


/**
 *
 *
 * @author 
 */
@Slf4j
public class H3cCfgParseTreeWalker {

    private H3cConfigNode currCfgNode = null;
    private final H3cConfigContext context;
    private final H3cSystemParser sysParser = new H3cSystemParser();
    private final H3cIntfParser intfParser = new H3cIntfParser();
    private final H3cIntfIpParser ipParser = new H3cIntfIpParser();
    private final H3cIntfIp6Parser ip6Parser = new H3cIntfIp6Parser();
    private final H3cBgpParser bgpParser = new H3cBgpParser();
    private final ObjectMapper jacksonMapper = new ObjectMapper();

    public H3cCfgParseTreeWalker(String mgmtIp, String vendor) {
        this.context = new H3cConfigContext(mgmtIp, vendor);
        this.currCfgNode = createRootConfigNode(context);
    }

    public H3cConfigModel getModel() {
        return context.getModel();
    }

    public void walk(IndentedRowConfigNode node) {

        enterFile(node);

        List<IndentedRowConfigNode> children = node.getChildren();
        if (children != null) {
            for (IndentedRowConfigNode child : children) {
                internalWalk(child);
            }
        }

        exitFile(node);
    }

    /**
     * 深度优先遍历AST tree
     * @param node tree node
     */
    private void internalWalk(IndentedRowConfigNode node) {
        enterRow(node);

        List<IndentedRowConfigNode> children = node.getChildren();
        if (children != null) {
            for (IndentedRowConfigNode child : children) {
                internalWalk(child);
            }
        }

        exitRow(node);
    }

    public void enterFile(IndentedRowConfigNode node) {

    }

    public void exitFile(IndentedRowConfigNode node) {

    }

    public void enterRow(IndentedRowConfigNode node) {

        IndentedRowCfgParser.RowContext ctx = node.getCtx();

        H3cConfigNode cfgNode = createConfigNode(currCfgNode, ctx);
        switch (cfgNode.getEntityType()) {
            case SYSTEM: {
                sysParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE: {
                intfParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE_IP: {
                ipParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE_IP6: {
                ip6Parser.parse(cfgNode, ctx);
                break;
            }
            case BGP: {
                bgpParser.parse(cfgNode, ctx);
                break;
            }
            default: {
                //
            }
        }

        currCfgNode = cfgNode;

    }

    public void exitRow(IndentedRowConfigNode node) {
        if (currCfgNode.getIsEntityRoot()) {
            H3cConfigContext context = currCfgNode.getContext();
            H3cConfigModel model = context.getModel();
            H3cConfigEntityType type = currCfgNode.getEntityType();

            Object entityInstance = context.remParsingEntity(type);
            switch (type) {
                case INTERFACE: {
                    Interface intf = (Interface) entityInstance;
                    model.putIntf(intf.getName(), intf);
                    break;
                }
                case INTERFACE_IP: {
                    Interface intf = (Interface) context.getParsingEntity(INTERFACE);
                    intf.getIps().add((InterfaceIp) entityInstance);
                    break;
                }
                case INTERFACE_IP6: {
                    Interface intf = (Interface) context.getParsingEntity(INTERFACE);
                    intf.getIp6s().add((InterfaceIp6) entityInstance);
                    break;
                }
                case BGP: {
                    Bgp bgp = (Bgp) entityInstance;
                    model.setBgp(bgp);
                    break;
                }
                default: {
                    //
                }
            }
        }

        currCfgNode = currCfgNode.getParent();
    }

    private H3cConfigNode createConfigNode(H3cConfigNode parent, IndentedRowCfgParser.RowContext ctx) {

        String word0 = ctx.WORD(0) == null ? "" : ctx.WORD(0).getText();
        String word1 = ctx.WORD(1) == null ? "" : ctx.WORD(1).getText();

        H3cConfigNode cfgNode = new H3cConfigNode();
        cfgNode.setParent(parent);
        cfgNode.setWord0(word0);
        cfgNode.setWord1(word1);
        cfgNode.setIsRoot(false);
        cfgNode.setContext(parent.getContext());

        if ("sysname".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(H3cConfigEntityType.SYSTEM);
        } else if ("version".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(H3cConfigEntityType.SYSTEM);
        } else if ("interface".equals(word0) && parent.getIsRoot()) {
            log.debug("{}", ctx.getText());
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(H3cConfigEntityType.INTERFACE);
        } else if ("bgp".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(H3cConfigEntityType.BGP);
        } else if ("ip".equals(word0) && "address".equals(word1)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == H3cConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(H3cConfigEntityType.INTERFACE_IP);
        } else if ("ipv6".equals(word0) && "address".equals(word1)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == H3cConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(H3cConfigEntityType.INTERFACE_IP6);
        } else if (parent.getIsRoot()) {
            // 暂不支持解析的一级节点
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(H3cConfigEntityType.UNKNOWN);
        } else {
            // 支持解析或不支持解析的非根节点
            cfgNode.setIsEntityRoot(false);
            // 继承父节点的
            cfgNode.setEntityType(parent.getEntityType());
        }

        return cfgNode;
    }


    private H3cConfigNode createRootConfigNode(H3cConfigContext context) {
        H3cConfigNode cfgNode = new H3cConfigNode();
        cfgNode.setParent(null);
        cfgNode.setWord0("root");
        cfgNode.setWord1("root");
        cfgNode.setIsRoot(true);
        cfgNode.setIsEntityRoot(false);
        cfgNode.setEntityType(H3cConfigEntityType.ROOT);
        cfgNode.setContext(context);
        return cfgNode;
    }

}
