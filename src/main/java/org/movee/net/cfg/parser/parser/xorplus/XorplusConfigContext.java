package org.movee.net.cfg.parser.parser.xorplus;

import lombok.Data;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Data
public class XorplusConfigContext {

    private String mgmtIp;
    private String vendor;

    // 解析出来的配置模型
    private XorplusConfigModel model;

    // 当前正在解析的 ConfigModelEntity,  type -> ConfigModelEntity
    private Map<XorplusConfigEntityType, Object> parsingEntities;

    public XorplusConfigContext(String mgmtIp, String vendor) {
        this.mgmtIp = mgmtIp;
        this.vendor = vendor;
        this.model = new XorplusConfigModel();
        this.model.setTimestamp(Instant.now().toEpochMilli());
        this.model.setMgmtIp(mgmtIp);
        this.model.getNetware().setMgmtIp(mgmtIp).setVendor(vendor);
        this.parsingEntities = new LinkedHashMap<>();
    }

    public Object getParsingEntity(XorplusConfigEntityType type) {
        return parsingEntities.get(type);
    }

    public void setParsingEntity(XorplusConfigEntityType type, Object entity) {
        parsingEntities.put(type, entity);
    }

    public Object remParsingEntity(XorplusConfigEntityType type) {
        return parsingEntities.remove(type);
    }
}
