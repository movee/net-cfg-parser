package org.movee.net.cfg.parser.parser.cisco;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

/**
 *
 *
 * @author 
 */
public class CiscoBgpPeerParser {

    public void parse(CiscoConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        CiscoConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            CiscoConfigModel.BgpPeer bgpPeer = new CiscoConfigModel.BgpPeer().setPeerIp(word1);

            context.setParsingEntity(CiscoConfigEntityType.BGP_PEER, bgpPeer);
        } else {
            CiscoConfigModel.BgpPeer bgpPeer = (CiscoConfigModel.BgpPeer) context.getParsingEntity(CiscoConfigEntityType.BGP_PEER);

            CiscoConfigNode parent = configNode.getParent();
            if ("remote-as".equals(word0) && parent.getIsEntityRoot()) {
                bgpPeer.setPeerAs(word1);
            }

        }

    }

}
