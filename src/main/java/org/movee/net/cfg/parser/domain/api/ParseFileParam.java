package org.movee.net.cfg.parser.domain.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ParseFileParam {
    @NotBlank
    private String mgmtIp;
    private Integer age;
    @NotNull
    @Min(10)
    @Max(100)
    private Integer value;
    @Min(30)
    private Integer id;
}
