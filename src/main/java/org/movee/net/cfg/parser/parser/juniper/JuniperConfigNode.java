package org.movee.net.cfg.parser.parser.juniper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JuniperConfigNode {

    // 父节点
    private JuniperConfigNode parent;
    // 节点配置类型
    private String word0 = "";
    // 节点配置名称
    private String word1 = "";

    private Boolean isRoot = false;
    // 是某个entity的根node
    private Boolean isEntityRoot = false;

    // 当前解析节点所属entity type
    private JuniperConfigEntityType entityType;

    private JuniperConfigContext context;

}
