package org.movee.net.cfg.parser.parser.huawei;

import lombok.Data;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Data
public class HuaweiConfigContext {

    private String mgmtIp;
    private String vendor;

    // 解析出来的配置模型
    private HuaweiConfigModel model;

    // 当前正在解析的 ConfigModelEntity,  type -> ConfigModelEntity
    private Map<HuaweiConfigEntityType, Object> parsingEntities;

    public HuaweiConfigContext(String mgmtIp, String vendor) {
        this.mgmtIp = mgmtIp;
        this.vendor = vendor;
        this.model = new HuaweiConfigModel();
        this.model.setMgmtIp(mgmtIp);
        this.model.setTimestamp(Instant.now().toEpochMilli());
        this.model.getNetware().setMgmtIp(mgmtIp).setVendor(vendor);
        this.parsingEntities = new LinkedHashMap<>();
    }

    public Object getParsingEntity(HuaweiConfigEntityType type) {
        return parsingEntities.get(type);
    }

    public void setParsingEntity(HuaweiConfigEntityType type, Object entity) {
        parsingEntities.put(type, entity);
    }

    public Object remParsingEntity(HuaweiConfigEntityType type) {
        return parsingEntities.remove(type);
    }
}
