package org.movee.net.cfg.parser.parser.cisco;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.utils.ParserUtils;

/**
 *
 *
 * @author 
 */
public class CiscoIntfIp6Parser {

    public void parse(CiscoConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        CiscoConfigContext context = configNode.getContext();
        if (configNode.getIsEntityRoot()) {

            String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();

            String[] segs = word2.split("/");
            String ipAddr = segs[0];
            Integer prefixLen = segs.length > 1 ? ParserUtils.parseNullableInteger(segs[1]) : null;

            CiscoConfigModel.InterfaceIp6 ip = new CiscoConfigModel.InterfaceIp6().setIpAddress(ipAddr).setPrefixLen(prefixLen);
            context.setParsingEntity(CiscoConfigEntityType.INTERFACE_IP6, ip);
        }

    }
}
