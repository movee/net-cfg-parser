package org.movee.net.cfg.parser.utils;

import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

public class ParserUtils {

    public static Long parseNullableLong(@Nullable String value) {
        if (StringUtils.hasLength(value)) {
            return Long.parseLong(value);
        }

        return null;
    }

    public static Integer parseNullableInteger(@Nullable String value) {
        if (StringUtils.hasLength(value)) {
            return Integer.parseInt(value);
        }

        return null;
    }

}
