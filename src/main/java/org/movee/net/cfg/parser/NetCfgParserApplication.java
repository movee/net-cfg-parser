package org.movee.net.cfg.parser;

import org.movee.net.cfg.parser.config.NetCfgProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties(NetCfgProperties.class)
public class NetCfgParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetCfgParserApplication.class, args);
    }

}
