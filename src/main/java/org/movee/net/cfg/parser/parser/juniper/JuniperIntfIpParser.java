package org.movee.net.cfg.parser.parser.juniper;

import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.juniper.JuniperConfigEntityType.INTERFACE_IP;
import static org.movee.net.cfg.parser.parser.juniper.JuniperConfigModel.InterfaceIp;

/**
 *
 *
 * @author 
 */
public class JuniperIntfIpParser {

    public void parse(JuniperConfigNode configNode) {

        JuniperConfigContext context = configNode.getContext();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {

            String[] segs = word1.split("/");
            String ipAddr = segs[0];
            Integer prefixLen = ParserUtils.parseNullableInteger(segs[1]);

            InterfaceIp ip = new InterfaceIp().setIpAddress(ipAddr).setPrefixLen(prefixLen);
            context.setParsingEntity(INTERFACE_IP, ip);
        }

    }

}
