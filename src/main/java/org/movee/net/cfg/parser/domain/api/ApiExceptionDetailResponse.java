package org.movee.net.cfg.parser.domain.api;

public class ApiExceptionDetailResponse<T> extends ApiExceptionResponse {

    private T exception;

    public ApiExceptionDetailResponse(String requestId, String code, String message, T exception) {
        super(requestId, code, message);
        this.exception = exception;
    }

}
