package org.movee.net.cfg.parser.domain.api;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 *
 * @author 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ApiException extends RuntimeException {

    private ApiStatusCode apiStatusCode;

    public ApiException(ApiStatusCode apiStatusCode, final String msg) {
        super(msg);
        this.apiStatusCode = apiStatusCode;
    }

}
