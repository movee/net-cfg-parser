package org.movee.net.cfg.parser.domain.parser;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IndentedRowConfigNode {

    private int offset = -1;
    private IndentedRowConfigNode parent;
    private List<IndentedRowConfigNode> children = new ArrayList<>();

    private IndentedRowCfgParser.RowContext ctx;

    public void appendChild(IndentedRowConfigNode child) {
        children.add(child);
    }
}
