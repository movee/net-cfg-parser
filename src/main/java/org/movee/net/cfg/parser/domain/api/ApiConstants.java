package org.movee.net.cfg.parser.domain.api;

public interface ApiConstants {

    // internal method associated with this api
    String API_REQUEST_ENTRY = "API_REQUEST_ENTRY";
    // api invoked time
    String API_REQUEST_START = "API_REQUEST_START";

    String X_API_REQUEST_ID = "x-api-request-id";
    String X_API_DATE = "x-api-date";

}
