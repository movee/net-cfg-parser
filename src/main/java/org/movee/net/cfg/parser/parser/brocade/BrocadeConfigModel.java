package org.movee.net.cfg.parser.parser.brocade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BrocadeConfigModel {

    private Long timestamp;
    private String mgmtIp;
    private Netware netware = new Netware();
    // Interface name --> Interface
    private Map<String, Interface> intfs = new HashMap<>();

    public Interface getIntf(String name) {
        return intfs.get(name);
    }

    public void putIntf(String name, Interface intf) {
        intfs.put(name, intf);
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Netware {

        private String mgmtIp;
        private String hostname;
        private String vendor;
        private String softwareVersion;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Interface {

        private String name;
        private String interfaceType;
        private String adminStatus;
        private String description;
        private Long bandwidth;
        private Long mtu;

        private List<InterfaceIp> ips = new ArrayList<>();
        private List<InterfaceVlan> vlans = new ArrayList<>();

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class InterfaceIp {

        private String ipAddress;
        private Integer prefixLen;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class InterfaceVlan {

        private String name;
        private Integer vlanId;

    }


}
