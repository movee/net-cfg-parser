package org.movee.net.cfg.parser.parser.xorplus;

import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigModel.Vlan;
import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigEntityType.VLAN;

/**
 *
 *
 * @author 
 */
public class XorplusVlanParser {

    public void parse(XorplusConfigNode configNode) {

        XorplusConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            Vlan vlan = new Vlan().setVlanId(ParserUtils.parseNullableInteger(word1));
            context.setParsingEntity(VLAN, vlan);
        } else {
            Vlan vlan = (Vlan) context.getParsingEntity(VLAN);

            XorplusConfigNode parent = configNode.getParent();
            if ("description".equals(word0) && parent.getIsEntityRoot()) {
                vlan.setDescription(word1);
            }

            if ("vlan-name".equals(word0) && parent.getIsEntityRoot()) {
                vlan.setDescription(word1);
            }

            if ("l3-interface".equals(word0) && parent.getIsEntityRoot()) {
                vlan.setDescription(word1);
            }
        }

    }

}
