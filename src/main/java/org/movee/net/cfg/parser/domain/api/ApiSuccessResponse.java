package org.movee.net.cfg.parser.domain.api;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 *
 * @author 
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class ApiSuccessResponse<T> extends ApiResponse {

    private T data;

    public ApiSuccessResponse(final T data) {
        super(ApiStatusCode.OK.getCode(), "success");
        this.data = data;
    }

    public ApiSuccessResponse(final String message, final T data) {
        super(ApiStatusCode.OK.getCode(), message);
        this.data = data;
    }

}
