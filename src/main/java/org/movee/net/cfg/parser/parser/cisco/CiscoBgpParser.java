package org.movee.net.cfg.parser.parser.cisco;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

/**
 *
 *
 * @author 
 */
public class CiscoBgpParser {

    public void parse(CiscoConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        CiscoConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
            CiscoConfigModel.Bgp bgp = new CiscoConfigModel.Bgp().setLocalAs(word2);

            context.setParsingEntity(CiscoConfigEntityType.BGP, bgp);
        } else {
            CiscoConfigModel.Bgp bgp = (CiscoConfigModel.Bgp) context.getParsingEntity(CiscoConfigEntityType.BGP);

            CiscoConfigNode parent = configNode.getParent();
            if ("router-id".equals(word0) && parent.getIsEntityRoot()) {
                bgp.setBgpId(word1);
            }

            if ("neighbor".equals(word0) && parent.getIsEntityRoot()) {
                String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
                String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();
                if ("as-number".equals(word2)) {
                    CiscoConfigModel.BgpPeer peer = new CiscoConfigModel.BgpPeer().setPeerIp(word1).setPeerAs(word3);
                    bgp.getPeers().put(peer.getPeerIp(), peer);
                }
            }
        }

    }

}
