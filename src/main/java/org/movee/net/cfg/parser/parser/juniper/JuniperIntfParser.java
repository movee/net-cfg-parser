package org.movee.net.cfg.parser.parser.juniper;

import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.juniper.JuniperConfigEntityType.INTERFACE;

/**
 *
 *
 * @author 
 */
public class JuniperIntfParser {

    public void parse(JuniperConfigNode configNode) {
        JuniperConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            JuniperConfigModel.Interface intf = new JuniperConfigModel.Interface().setName(word0);

            context.setParsingEntity(INTERFACE, intf);
        } else {
            JuniperConfigModel.Interface intf = (JuniperConfigModel.Interface) context.getParsingEntity(INTERFACE);

            JuniperConfigNode parent = configNode.getParent();

            if ("mtu".equals(word0) && parent.getIsEntityRoot()) {
                intf.setMtu(ParserUtils.parseNullableLong(word1));
            }

            if ("description".equals(word0) && parent.getIsEntityRoot()) {
                intf.setDescription(word1);
            }

            if ("802.3ad".equals(word0) && "gigether-options".equals(parent.getWord0())) {
                intf.setAd8023(word1);
            }


        }
    }

}
