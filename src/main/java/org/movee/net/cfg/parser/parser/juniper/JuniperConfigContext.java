package org.movee.net.cfg.parser.parser.juniper;

import lombok.Data;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Data
public class JuniperConfigContext {

    private String mgmtIp;
    private String vendor;

    // 解析出来的配置模型
    private JuniperConfigModel model;

    // 当前正在解析的 ConfigModelEntity,  type -> ConfigModelEntity
    private Map<JuniperConfigEntityType, Object> parsingEntities;

    public JuniperConfigContext(String mgmtIp, String vendor) {
        this.mgmtIp = mgmtIp;
        this.vendor = vendor;
        this.model = new JuniperConfigModel();
        this.model.setTimestamp(Instant.now().toEpochMilli());
        this.model.setMgmtIp(mgmtIp);
        this.model.getNetware().setMgmtIp(mgmtIp).setVendor(vendor);
        this.parsingEntities = new LinkedHashMap<>();
    }

    public Object getParsingEntity(JuniperConfigEntityType type) {
        return parsingEntities.get(type);
    }

    public void setParsingEntity(JuniperConfigEntityType type, Object entity) {
        parsingEntities.put(type, entity);
    }

    public Object remParsingEntity(JuniperConfigEntityType type) {
        return parsingEntities.remove(type);
    }

}
