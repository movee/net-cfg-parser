package org.movee.net.cfg.parser.parser.xorplus;

import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigModel.VlanInterface;
import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigEntityType.VLAN_INTERFACE;


/**
 *
 *
 * @author 
 */
public class XorplusVlanIntfParser {

    public void parse(XorplusConfigNode configNode) {
        XorplusConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            VlanInterface vlanIntf = new VlanInterface().setName(word1);
            context.setParsingEntity(VLAN_INTERFACE, vlanIntf);
        } else {
            VlanInterface vlanIntf = (VlanInterface) context.getParsingEntity(VLAN_INTERFACE);

            XorplusConfigNode parent = configNode.getParent();
            if ("description".equals(word0) && parent.getIsEntityRoot()) {
                vlanIntf.setDescription(word1);
            }

            if ("mtu".equals(word0) && parent.getIsEntityRoot()) {
                vlanIntf.setMtu(ParserUtils.parseNullableLong(word1));
            }

            if ("interface-mac".equals(word0) && parent.getIsEntityRoot()) {
                vlanIntf.setDescription(word1);
            }
        }

    }

}
