package org.movee.net.cfg.parser.parser.xorplus;

import org.movee.net.cfg.parser.domain.model.InterfaceType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Getter
@AllArgsConstructor
public enum XorplusInterfaceType {

    GIGABIT_ETHERNET("gigabit-ethernet", InterfaceType.PHYSICAL),
    AGGREGATE_ETHERNET("aggregate-ethernet", InterfaceType.AGGREGATOR);

    private final String name;
    private final InterfaceType type;

    private static final Map<String, XorplusInterfaceType> MAP = new HashMap<>();

    static {
        for (XorplusInterfaceType type : XorplusInterfaceType.values()) {
            MAP.put(type.getName(), type);
        }
    }

    public static XorplusInterfaceType fromName(String name) {
        return MAP.get(name);
    }

}
