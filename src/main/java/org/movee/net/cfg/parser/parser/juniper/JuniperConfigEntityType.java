package org.movee.net.cfg.parser.parser.juniper;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 *
 * @author 
 */
@Getter
@AllArgsConstructor
public enum JuniperConfigEntityType {

    ROOT(""),

    SYSTEM("system"),

    INTERFACES("interfaces"),

    INTERFACE("ethernet"),
    SUB_INTERFACE("unit"),
    INTERFACE_IP("inet"),
    INTERFACE_IP6("inet6"),

    UNKNOWN("unknown");

    private final String key;

}
