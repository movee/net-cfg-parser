package org.movee.net.cfg.parser.parser.xorplus;

import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigModel.VlanInterfaceIp;
import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigEntityType.VLAN_INTERFACE_IP;

/**
 *
 *
 * @author 
 */
public class XorplusVlanIntfIpParser {

    public void parse(XorplusConfigNode configNode) {

        XorplusConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            VlanInterfaceIp ip = new VlanInterfaceIp().setIpAddress(word1);
            context.setParsingEntity(VLAN_INTERFACE_IP, ip);
        } else {
            VlanInterfaceIp ip = (VlanInterfaceIp) context.getParsingEntity(VLAN_INTERFACE_IP);

            XorplusConfigNode parent = configNode.getParent();
            if ("prefix-length".equals(word0) && parent.getIsEntityRoot()) {
                ip.setPrefixLen(ParserUtils.parseNullableInteger(word1));
            }
        }
    }
}
