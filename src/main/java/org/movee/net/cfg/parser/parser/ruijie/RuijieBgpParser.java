package org.movee.net.cfg.parser.parser.ruijie;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

import static org.movee.net.cfg.parser.parser.ruijie.RuijieConfigEntityType.BGP;

/**
 *
 *
 * @author 
 */
public class RuijieBgpParser {

    public void parse(RuijieConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        RuijieConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            RuijieConfigModel.Bgp bgp = new RuijieConfigModel.Bgp().setLocalAs(word1);

            context.setParsingEntity(BGP, bgp);
        } else {
            RuijieConfigModel.Bgp bgp = (RuijieConfigModel.Bgp) context.getParsingEntity(BGP);

            RuijieConfigNode parent = configNode.getParent();
            if ("router-id".equals(word0) && parent.getIsEntityRoot()) {
                bgp.setBgpId(word1);
            }

            if ("peer".equals(word0) && parent.getIsEntityRoot()) {
                String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
                String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();
                if ("as-number".equals(word2)) {
                    RuijieConfigModel.BgpPeer peer = new RuijieConfigModel.BgpPeer().setPeerIp(word1).setPeerAs(word3);
                    bgp.getPeers().put(peer.getPeerIp(), peer);
                }
            }
        }

    }

}
