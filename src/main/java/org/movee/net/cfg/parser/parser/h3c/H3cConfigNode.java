package org.movee.net.cfg.parser.parser.h3c;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class H3cConfigNode {

    // 父节点
    private H3cConfigNode parent;

    private String word0;
    private String word1;

    private Boolean isRoot = false;
    // 是某个entity的根node
    private Boolean isEntityRoot = false;

    // 当前解析节点所属的entity type
    private H3cConfigEntityType entityType;

    private H3cConfigContext context;

}
