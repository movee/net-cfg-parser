package org.movee.net.cfg.parser.parser.brocade;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.brocade.BrocadeConfigEntityType.INTERFACE_IP;
import static org.movee.net.cfg.parser.parser.brocade.BrocadeConfigModel.InterfaceIp;

/**
 *
 *
 * @author 
 */
public class BrocadeIntfIpParser {

    public void parse(BrocadeConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        BrocadeConfigContext context = configNode.getContext();
        String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();

        if (configNode.getIsEntityRoot()) {
            String[] segs = word2.split("/");
            String ipAddr = segs[0];
            Integer prefixLen = ParserUtils.parseNullableInteger(segs[1]);

            InterfaceIp ip = new InterfaceIp().setIpAddress(ipAddr).setPrefixLen(prefixLen);
            context.setParsingEntity(INTERFACE_IP, ip);
        }

    }

}
