package org.movee.net.cfg.parser.parser.sonic;

import org.movee.net.cfg.parser.domain.model.ConfigModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class SonicConfigModelConverter implements Converter<SonicConfigModel, ConfigModel> {

    @Nullable
    @Override
    public ConfigModel convert(@NonNull SonicConfigModel source) {
        return null;
    }
}
