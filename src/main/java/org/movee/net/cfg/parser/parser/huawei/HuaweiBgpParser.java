package org.movee.net.cfg.parser.parser.huawei;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigModel.Bgp;
import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigModel.BgpPeer;


/**
 *
 *
 * @author 
 */
public class HuaweiBgpParser {

    public void parse(HuaweiConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        HuaweiConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            Bgp bgp = new Bgp().setLocalAs(word1);

            context.setParsingEntity(HuaweiConfigEntityType.BGP, bgp);
        } else {
            Bgp bgp = (Bgp) context.getParsingEntity(HuaweiConfigEntityType.BGP);

            HuaweiConfigNode parent = configNode.getParent();
            if ("router-id".equals(word0) && parent.getIsEntityRoot()) {
                bgp.setBgpId(word1);
            }

            if ("peer".equals(word0) && parent.getIsEntityRoot()) {
                String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
                String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();
                if ("as-number".equals(word2)) {
                    BgpPeer peer = new BgpPeer().setPeerIp(word1).setPeerAs(word3);
                    bgp.getPeers().put(peer.getPeerIp(), peer);
                }
            }
        }

    }

}
