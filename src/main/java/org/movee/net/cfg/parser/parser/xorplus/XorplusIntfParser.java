package org.movee.net.cfg.parser.parser.xorplus;


import org.movee.net.cfg.parser.domain.model.InterfaceStatus;
import org.movee.net.cfg.parser.utils.ParserUtils;
import lombok.extern.slf4j.Slf4j;

import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigModel.Interface;
import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigEntityType.INTERFACE;


/**
 *
 *
 * @author 
 */
@Slf4j
public class XorplusIntfParser {

    public void parse(XorplusConfigNode configNode) {
        XorplusConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        XorplusInterfaceType intfType = XorplusInterfaceType.fromName(word0);
        if (intfType != null) {
            Interface intf = new XorplusConfigModel.Interface()
                    .setName(word1)
                    .setInterfaceType(intfType.getType().name());

            context.setParsingEntity(INTERFACE, intf);
        } else {
            Interface intf = (Interface) context.getParsingEntity(INTERFACE);

            log.debug("ifname: {}, word0: {}, word1: {}", intf.getName(), word0, word1);
            XorplusConfigNode parent = configNode.getParent();
            if ("disable".equals(word0) && parent.getIsEntityRoot()) {
                if ("false".equals(word1)) {
                    intf.setAdminStatus(InterfaceStatus.UP.name());
                } else if ("true".equals(word1)) {
                    intf.setAdminStatus(InterfaceStatus.DOWN.name());
                }
            }

            if ("mtu".equals(word0) && parent.getIsEntityRoot()) {
                intf.setMtu(ParserUtils.parseNullableLong(word1));
            }

            if ("speed".equals(word0) && parent.getIsEntityRoot()) {
                intf.setBandwidth(ParserUtils.parseNullableLong(word1));
            }

            if ("description".equals(word0) && parent.getIsEntityRoot()) {
                intf.setDescription(word1);
            }

            if ("802.3ad".equals(word0) && "ether-options".equals(parent.getWord0())) {
                intf.setAd8023(word1);
            }

            if ("native-vlan-id".equals(word0) && "ethernet-switching".equals(parent.getWord0())) {
                intf.setNativeVlanId(ParserUtils.parseNullableInteger(word1));
            }
        }

    }



}
