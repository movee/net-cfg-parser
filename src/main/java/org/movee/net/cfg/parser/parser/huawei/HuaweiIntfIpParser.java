package org.movee.net.cfg.parser.parser.huawei;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigModel.InterfaceIp;

/**
 *
 *
 * @author 
 */
public class HuaweiIntfIpParser {

    public void parse(HuaweiConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        HuaweiConfigContext context = configNode.getContext();
        if (configNode.getIsEntityRoot()) {
            String word0 = configNode.getWord0();
            String word1 = configNode.getWord1();
            String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
            String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();

            if ("ip".equals(word0) && "address".equals(word1)) {
                InterfaceIp ip = new InterfaceIp().setIpAddress(word2).setMask(word3).setIsStackPeer(false);
                context.setParsingEntity(HuaweiConfigEntityType.INTERFACE_IP, ip);
            } else if ("ip".equals(word2) && "address".equals(word3)) {
                String word4 = ctx.WORD(4) == null ? "" : ctx.WORD(4).getText();
                String word5 = ctx.WORD(5) == null ? "" : ctx.WORD(5).getText();
                InterfaceIp ip = new InterfaceIp().setIpAddress(word4).setMask(word5).setIsStackPeer(true);
                context.setParsingEntity(HuaweiConfigEntityType.INTERFACE_IP, ip);
            }
        }

    }
}
