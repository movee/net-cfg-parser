package org.movee.net.cfg.parser.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Getter
@AllArgsConstructor
public enum Vendor {

    HUAWEI("huawei"),
    H3C("h3c"),
    CISCO("cisco"),
    JUNIPER("juniper"),
    XORPLUS("xorplus"),
    LIBRA("libra"),
    BROCADE("brocade");

    private final String value;

    private final static Map<String, Vendor> MAP = new HashMap<>();

    static {
        for (Vendor v : Vendor.values()) {
            MAP.put(v.value, v);
        }
    }

    public static Vendor fromName(String name) {
        return MAP.get(name);
    }

}
