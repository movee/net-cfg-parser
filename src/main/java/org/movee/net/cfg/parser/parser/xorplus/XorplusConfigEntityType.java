package org.movee.net.cfg.parser.parser.xorplus;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 *
 * @author 
 */
@Getter
@AllArgsConstructor
public enum XorplusConfigEntityType {

    ROOT(""),

    SYSTEM("system"),

    VLANS("vlan"),
    VLAN_INTERFACES("vlan-interface"),
    INTERFACES("interface"),

    BGP("bgp"),
    BGP_PEER("peer"),

    VLAN("vlan-id"),
    VLAN_INTERFACE("interface"),
    INTERFACE("ethernet"),
    VLAN_INTERFACE_IP("address"),

    UNKNOWN("unknown");

    private final String key;

}
