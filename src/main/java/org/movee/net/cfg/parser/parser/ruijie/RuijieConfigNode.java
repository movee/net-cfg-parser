package org.movee.net.cfg.parser.parser.ruijie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class RuijieConfigNode {

    // 父节点
    private RuijieConfigNode parent;

    private String word0;
    private String word1;

    private Boolean isRoot = false;
    // 是某个entity的根node
    private Boolean isEntityRoot = false;

    // 当前解析节点所属的entity type
    private RuijieConfigEntityType entityType;

    private RuijieConfigContext context;

}
