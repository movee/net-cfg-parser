package org.movee.net.cfg.parser.parser.ruijie;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.domain.model.InterfaceStatus;
import org.movee.net.cfg.parser.utils.ParserUtils;

/**
 *
 *
 * @author 
 */
public class RuijieIntfParser {

    public void parse(RuijieConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        RuijieConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
            RuijieConfigModel.Interface intf = new RuijieConfigModel.Interface().setName(String.join(" ", word1, word2))
                    .setAdminStatus(InterfaceStatus.UP.name());

            context.setParsingEntity(RuijieConfigEntityType.INTERFACE, intf);
        } else {
            RuijieConfigModel.Interface intf = (RuijieConfigModel.Interface) context.getParsingEntity(RuijieConfigEntityType.INTERFACE);

            RuijieConfigNode parent = configNode.getParent();
            if ("description".equals(word0) && parent.getIsEntityRoot()) {
                intf.setDescription(word1);
            }
            if ("shutdown".equals(word0) && parent.getIsEntityRoot()) {
                intf.setAdminStatus(InterfaceStatus.DOWN.name());
            }
            if ("mtu".equals(word0) && parent.getIsEntityRoot()) {
                intf.setMtu(ParserUtils.parseNullableLong(word1));
            }
        }

    }

}
