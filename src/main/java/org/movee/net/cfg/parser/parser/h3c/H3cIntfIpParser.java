package org.movee.net.cfg.parser.parser.h3c;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

import static org.movee.net.cfg.parser.parser.h3c.H3cConfigModel.InterfaceIp;


/**
 *
 *
 * @author 
 */
public class H3cIntfIpParser {

    public void parse(H3cConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        H3cConfigContext context = configNode.getContext();

        if (configNode.getIsEntityRoot()) {
            String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
            String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();

            InterfaceIp ip = new InterfaceIp().setIpAddress(word2).setIpMask(word3).setIsStackPeer(false);

            // 解析堆叠
            String word4 = ctx.WORD(4) == null ? "" : ctx.WORD(4).getText();
            if ("irf-member".equals(word4)) {
                ip.setIsStackPeer(true);
            }

            context.setParsingEntity(H3cConfigEntityType.INTERFACE_IP, ip);
        }

    }

}
