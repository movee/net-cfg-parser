package org.movee.net.cfg.parser.antlr;

import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import java.util.Collections;
import java.util.List;

@Slf4j
public class CfgErrorListener extends BaseErrorListener {

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer,
                            Object offendingSymbol,
                            int line,
                            int charPositionInLine,
                            String msg,
                            RecognitionException e) {

        List<String> stack = ((Parser) recognizer).getRuleInvocationStack();
        Collections.reverse(stack);
        log.warn("rule stack: {}", stack);
        log.warn("unexpect configuration file format. rule stack: {}. line {}:{} at {}: {}",
                stack, line, charPositionInLine, offendingSymbol, msg);

    }
}
