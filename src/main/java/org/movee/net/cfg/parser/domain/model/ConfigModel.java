package org.movee.net.cfg.parser.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ConfigModel {

    private Long timestamp;
    private String mgmtIp;
    private Netware netware;
    // name --> InterfaceUnit
    private Map<String, InterfaceUnit> interfaces = new TreeMap<>();
    // ip --> IpUnit
    private Map<String, IpUnit> ips = new TreeMap<>();

    private Bgp bgp = null;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    @JsonPropertyOrder({"interface", "pInterface", "cInterfaces", "ipv4s", "ipv6s"})
    public static class InterfaceUnit {

        @JsonProperty("pInterface")
        private String pIntf;
        @JsonProperty("interface")
        private Interface intf;
        @JsonProperty("cInterfaces")
        private Set<String> cIntfs = new TreeSet<>();
        private Set<String> ipv4s = new TreeSet<>();
        private Set<String> ipv6s = new TreeSet<>();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class IpUnit {

        private Ip ip;
        @JsonProperty("interface")
        private String intf;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Netware {

        private String mgmtIp;
        private String hostname;
        private String vendor;
        private String softwareVersion;
        private String stackPeer;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Interface {

        private String name;
        private String description;
        private String interfaceType;
        private String adminStatus;
        private Long bandwidth;
        private Long mtu;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Ip {

        private String ipAddress;
        @Deprecated
        private String ipMask;
        private Integer prefixLen;
        private String area;
        private String family;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Bgp {

        private String bgpId;
        private String localAs;
        private Long localPref;
        private Long med;
        private Map<String, BgpPeer> peers = new TreeMap<>();
        private List<String> inet4Prefixes = new ArrayList<>();
        private List<String> inet6Prefixes = new ArrayList<>();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class BgpPeer {
        private String localIp;
        private String peerAs;
        private String peerIp;

    }
}
