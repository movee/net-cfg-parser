package org.movee.net.cfg.parser.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 *
 * @author 
 */
@Getter
@AllArgsConstructor
public enum InterfaceStatus {
    UP(1),
    DOWN(2),
    TESTING(3);

    private int status;


}
