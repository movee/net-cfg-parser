package org.movee.net.cfg.parser.parser.xorplus;


import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigEntityType.BGP_PEER;
import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigModel.BgpPeer;

/**
 *
 *
 * @author 
 */
public class XorplusBgpPeerParser {

    public void parse(XorplusConfigNode configNode) {

        XorplusConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            BgpPeer bgpPeer = new BgpPeer().setPeerIp(word1);
            context.setParsingEntity(BGP_PEER, bgpPeer);
        } else {
            BgpPeer bgpPeer = (BgpPeer) context.getParsingEntity(BGP_PEER);

            XorplusConfigNode parent = configNode.getParent();
            if ("local-ip".equals(word0) && parent.getIsEntityRoot() && parent.getEntityType() == BGP_PEER) {
                bgpPeer.setLocalIp(word1);
            }

            if ("as".equals(word0) && parent.getIsEntityRoot() && parent.getEntityType() == BGP_PEER) {
                bgpPeer.setPeerAs(word1);
            }

        }

    }
}
