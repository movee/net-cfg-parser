package org.movee.net.cfg.parser.parser.ruijie;

import lombok.Data;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Data
public class RuijieConfigContext {

    private String mgmtIp;
    private String vendor;

    // 解析出来的配置模型
    private RuijieConfigModel model;

    // 当前正在解析的 ConfigModelEntity,  type -> ConfigModelEntity
    private Map<RuijieConfigEntityType, Object> parsingEntities;

    public RuijieConfigContext(String mgmtIp, String vendor) {
        this.mgmtIp = mgmtIp;
        this.vendor = vendor;
        this.model = new RuijieConfigModel();
        this.model.setMgmtIp(mgmtIp);
        this.model.setTimestamp(Instant.now().toEpochMilli());
        this.model.getNetware().setMgmtIp(mgmtIp).setVendor(vendor);
        this.parsingEntities = new LinkedHashMap<>();
    }

    public Object getParsingEntity(RuijieConfigEntityType type) {
        return parsingEntities.get(type);
    }

    public void setParsingEntity(RuijieConfigEntityType type, Object entity) {
        parsingEntities.put(type, entity);
    }

    public Object remParsingEntity(RuijieConfigEntityType type) {
        return parsingEntities.remove(type);
    }

}
