package org.movee.net.cfg.parser.parser.cisco;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.domain.model.InterfaceStatus;
import org.movee.net.cfg.parser.utils.ParserUtils;

/**
 *
 *
 * @author 
 */
public class CiscoIntfParser {

    public void parse(CiscoConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        CiscoConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            CiscoConfigModel.Interface intf = new CiscoConfigModel.Interface().setName(word1).setAdminStatus(InterfaceStatus.DOWN.name());

            context.setParsingEntity(CiscoConfigEntityType.INTERFACE, intf);
        } else {
            CiscoConfigModel.Interface intf = (CiscoConfigModel.Interface) context.getParsingEntity(CiscoConfigEntityType.INTERFACE);

            CiscoConfigNode parent = configNode.getParent();
            if ("description".equals(word0) && parent.getIsEntityRoot()) {
                intf.setDescription(word1);
            }
            if ("no".equals(word0) && "shutdown".equals(word1) && parent.getIsEntityRoot()) {
                intf.setAdminStatus(InterfaceStatus.UP.name());
            }
            if ("mtu".equals(word0) && parent.getIsEntityRoot()) {
                intf.setMtu(ParserUtils.parseNullableLong(word1));
            }
        }

    }

}
