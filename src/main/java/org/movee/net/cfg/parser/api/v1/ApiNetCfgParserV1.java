package org.movee.net.cfg.parser.api.v1;

import org.movee.net.cfg.parser.domain.api.ApiResponse;
import org.movee.net.cfg.parser.domain.api.ApiSuccessResponse;
import org.movee.net.cfg.parser.domain.api.ParseFileParam;
import org.movee.net.cfg.parser.service.ConfigFileParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Size;

/**
 * restfull api service for Topology Graph
 *
 * @author 
 */
@Validated
@RestController
@RequestMapping(value = "/v1", produces = "application/json")
@Slf4j
@Api(tags = "net-cfg-parser-v1")
public class ApiNetCfgParserV1 {

    private ObjectMapper jackson = new ObjectMapper();
    private final ConfigFileParser fileParser;

    public ApiNetCfgParserV1(ConfigFileParser fileParser) {
        this.fileParser = fileParser;
    }

    @ApiOperation("parse-file")
    @GetMapping(value = "/config/file")
    public ApiResponse parseFile(@ApiParam("设备管理ip")
                                     @Size(min = 7, max = 15)
                                     @RequestParam String mgmtIp,
                                 @RequestParam(required = false) String iaz) {
        return new ApiSuccessResponse<>(fileParser.parseConfigFile(mgmtIp, iaz));
    }

    @ApiOperation("parse-file2")
    @GetMapping(value = "/config/file2/{id}")
    public ApiResponse parseFile2(@Validated @ModelAttribute ParseFileParam param) {
        log.info("{}", param);
        return new ApiSuccessResponse<>("success");
    }

}
