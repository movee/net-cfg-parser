package org.movee.net.cfg.parser.parser.huawei;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HuaweiConfigNode {

    // 父节点
    private HuaweiConfigNode parent;

    private String word0;
    private String word1;

    private Boolean isRoot = false;
    // 是某个entity的根node
    private Boolean isEntityRoot = false;

    // 当前解析节点所属的entity type
    private HuaweiConfigEntityType entityType;

    private HuaweiConfigContext context;
}
