package org.movee.net.cfg.parser.utils;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public interface OrikaSupplier {
    MapperFacade ORIKA = new DefaultMapperFactory.Builder().build().getMapperFacade();
}
