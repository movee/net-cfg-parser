package org.movee.net.cfg.parser.api.utils;

import org.movee.net.cfg.parser.domain.api.ApiException;
import org.movee.net.cfg.parser.domain.api.ApiStatusCode;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.util.Collection;

/**
 * compare two topo domain entity field value
 *
 * @author 
 */
public class ApiAssert {

    public static void isTrue(boolean expression, ApiStatusCode code, String message) {
        if (!expression) {
            throw new ApiException(code, message);
        }
    }

    public static void notNull(@Nullable Object object, ApiStatusCode code, String message) {
        if (object == null) {
            throw new ApiException(code, message);
        }
    }

    public static void hasText(@Nullable String text, ApiStatusCode code, String message) {
        if (!StringUtils.hasText(text)) {
            throw new ApiException(code, message);
        }
    }

    public static void notEmpty(Collection<?> collection, ApiStatusCode code, String message) {
        if (collection == null || collection.isEmpty()) {
            throw new ApiException(code, message);
        }
    }
}

