package org.movee.net.cfg.parser.domain.api;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ApiExceptionResponse extends ApiResponse {

    private String requestId;

    public ApiExceptionResponse(String requestId, String code, String message) {
        super(code, message);
        this.requestId = requestId;
    }

}
