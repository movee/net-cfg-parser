package org.movee.net.cfg.parser.parser.h3c;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

import static org.movee.net.cfg.parser.parser.h3c.H3cConfigEntityType.BGP;
import static org.movee.net.cfg.parser.parser.h3c.H3cConfigModel.Bgp;
import static org.movee.net.cfg.parser.parser.h3c.H3cConfigModel.BgpPeer;

/**
 *
 *
 * @author 
 */
public class H3cBgpParser {

    public void parse(H3cConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        H3cConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            Bgp bgp = new Bgp().setLocalAs(word1);

            context.setParsingEntity(BGP, bgp);
        } else {
            Bgp bgp = (Bgp) context.getParsingEntity(BGP);

            H3cConfigNode parent = configNode.getParent();
            if ("router-id".equals(word0) && parent.getIsEntityRoot()) {
                bgp.setBgpId(word1);
            }

            if ("peer".equals(word0) && parent.getIsEntityRoot()) {
                String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();
                String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();
                if ("as-number".equals(word2)) {
                    BgpPeer peer = new BgpPeer().setPeerIp(word1).setPeerAs(word3);
                    bgp.getPeers().put(peer.getPeerIp(), peer);
                }
            }
        }

    }


}
