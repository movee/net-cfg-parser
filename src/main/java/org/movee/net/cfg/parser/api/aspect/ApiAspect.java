package org.movee.net.cfg.parser.api.aspect;

import io.github.resilience4j.ratelimiter.RateLimiterRegistry;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * restfull api service for Topology Graph
 *
 * @author 
 */
@Component
@Aspect
public class ApiAspect extends BasicApiAspect {

    @Autowired
    public ApiAspect(RateLimiterRegistry rateLimiterRegistry) {
        setRateLimiter(rateLimiterRegistry);
    }

    @Pointcut("execution(public * com.example.nconf.parser.api.v1.*.*(..))")
    public void apiPointCut() { }


    @Override
    @Around("apiPointCut()")
    public Object apiAroundAdvise(final ProceedingJoinPoint jp) throws Throwable {
        return super.apiAroundAdvise(jp);
    }

}

