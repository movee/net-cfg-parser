package org.movee.net.cfg.parser.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 *
 * @author 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties("nconf-parser")
public class NetCfgProperties {

    private String confBasePath;

}
