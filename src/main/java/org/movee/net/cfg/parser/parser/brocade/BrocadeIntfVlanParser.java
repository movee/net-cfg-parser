package org.movee.net.cfg.parser.parser.brocade;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.brocade.BrocadeConfigEntityType.INTERFACE_VLAN;
import static org.movee.net.cfg.parser.parser.brocade.BrocadeConfigModel.InterfaceVlan;

/**
 *
 *
 * @author 
 */
public class BrocadeIntfVlanParser {

    public void parse(BrocadeConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        BrocadeConfigContext context = configNode.getContext();
        String word3 = ctx.WORD(3) == null ? "" : ctx.WORD(3).getText();

        if (configNode.getIsEntityRoot()) {
            Integer vlanId = ParserUtils.parseNullableInteger(word3);

            InterfaceVlan vlan = new InterfaceVlan().setName("Vlan " + vlanId).setVlanId(vlanId);
            context.setParsingEntity(INTERFACE_VLAN, vlan);
        }

    }

}
