package org.movee.net.cfg.parser.parser.juniper;


/**
 *
 *
 * @author 
 */
public class JuniperSystemParser {

    public void parse(JuniperConfigNode configNode) {
        JuniperConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        JuniperConfigNode parent = configNode.getParent();
        if (parent.getIsEntityRoot()) {
            JuniperConfigModel.Netware netware = context.getModel().getNetware();
            if ("host-name".equals(word0)) {
                netware.setHostname(word1);
            }

        }
    }

}
