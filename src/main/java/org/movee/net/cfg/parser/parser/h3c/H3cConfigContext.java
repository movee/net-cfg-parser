package org.movee.net.cfg.parser.parser.h3c;

import lombok.Data;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Data
public class H3cConfigContext {

    private String mgmtIp;
    private String vendor;

    // 解析出来的配置模型
    private H3cConfigModel model;

    // 当前正在解析的 ConfigModelEntity,  type -> ConfigModelEntity
    private Map<H3cConfigEntityType, Object> parsingEntities;

    public H3cConfigContext(String mgmtIp, String vendor) {
        this.mgmtIp = mgmtIp;
        this.vendor = vendor;
        this.model = new H3cConfigModel();
        this.model.setMgmtIp(mgmtIp);
        this.model.setTimestamp(Instant.now().toEpochMilli());
        this.model.getNetware().setMgmtIp(mgmtIp).setVendor(vendor);
        this.parsingEntities = new LinkedHashMap<>();
    }

    public Object getParsingEntity(H3cConfigEntityType type) {
        return parsingEntities.get(type);
    }

    public void setParsingEntity(H3cConfigEntityType type, Object entity) {
        parsingEntities.put(type, entity);
    }

    public Object remParsingEntity(H3cConfigEntityType type) {
        return parsingEntities.remove(type);
    }


}
