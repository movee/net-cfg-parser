package org.movee.net.cfg.parser.domain.model;

public enum AddressFamilyType {

    IPV4,
    IPV6;

}
