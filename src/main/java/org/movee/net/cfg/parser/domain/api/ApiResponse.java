package org.movee.net.cfg.parser.domain.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse {

    private String code;
    private String message;
}
