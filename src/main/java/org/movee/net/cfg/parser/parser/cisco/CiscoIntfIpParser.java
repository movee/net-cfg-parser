package org.movee.net.cfg.parser.parser.cisco;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.cisco.CiscoConfigModel.InterfaceIp;
import static org.movee.net.cfg.parser.parser.cisco.CiscoConfigEntityType.INTERFACE_IP;

/**
 *
 *
 * @author 
 */
public class CiscoIntfIpParser {

    public void parse(CiscoConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        CiscoConfigContext context = configNode.getContext();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {

            String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();

            String[] segs = word2.split("/");
            String ipAddr = segs[0];
            Integer prefixLen = ParserUtils.parseNullableInteger(segs[1]);

            InterfaceIp ip = new InterfaceIp().setIpAddress(ipAddr).setPrefixLen(prefixLen);
            context.setParsingEntity(INTERFACE_IP, ip);
        }

    }

}
