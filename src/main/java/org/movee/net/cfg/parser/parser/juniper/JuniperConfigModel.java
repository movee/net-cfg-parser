package org.movee.net.cfg.parser.parser.juniper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 *
 * @author 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JuniperConfigModel {

    private Long timestamp;
    private String mgmtIp;
    private Netware netware = new Netware();
    // Interface name --> Interface
    private Map<String, Interface> intfs = new HashMap<>();
    private Bgp bgp;

    public Interface getIntf(String name) {
        return intfs.get(name);
    }

    public void putIntf(String name, Interface intf) {
        intfs.put(name, intf);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Netware {

        private String mgmtIp;
        private String hostname;
        private String vendor;
        private String softwareVersion;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Interface {

        private String name;
        private String interfaceType;
        private String adminStatus;
        private String description;
        private Long bandwidth;
        private Long mtu;
        // 所属的聚合口名字
        private String ad8023;

        private List<InterfaceIp> ips = new ArrayList<>();
        private List<InterfaceIp6> ip6s = new ArrayList<>();

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class InterfaceIp {

        private String ipAddress;
        private Integer prefixLen;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class InterfaceIp6 {

        private String ipAddress;
        private Integer prefixLen;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class Bgp {

        private String bgpId;
        private String localAs;
        private Long localPref;
        private Long med;
        private Map<String, BgpPeer> peers = new TreeMap<>();
        private List<String> inet4Prefixes = new ArrayList<>();
        private List<String> inet6Prefixes = new ArrayList<>();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Accessors(chain = true)
    public static class BgpPeer {
        private String localIp;
        private String peerAs;
        private String peerIp;

    }

}
