package org.movee.net.cfg.parser.parser.huawei;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.domain.parser.IndentedRowConfigNode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigEntityType.INTERFACE;
import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigModel.Bgp;
import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigModel.Interface;
import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigModel.InterfaceIp;
import static org.movee.net.cfg.parser.parser.huawei.HuaweiConfigModel.InterfaceIp6;

/**
 *
 *
 * @author 
 */
@Slf4j
public class HuaweiCfgParseTreeWalker {

    private HuaweiConfigNode currCfgNode = null;
    private final HuaweiConfigContext context;
    private final HuaweiSystemParser sysParser = new HuaweiSystemParser();
    private final HuaweiIntfParser intfParser = new HuaweiIntfParser();
    private final HuaweiIntfIpParser ipParser = new HuaweiIntfIpParser();
    private final HuaweiIntfIp6Parser ip6Parser = new HuaweiIntfIp6Parser();
    private final HuaweiBgpParser bgpParser = new HuaweiBgpParser();
    private final ObjectMapper jacksonMapper = new ObjectMapper();

    public HuaweiCfgParseTreeWalker(String mgmtIp, String vendor) {
        this.context = new HuaweiConfigContext(mgmtIp, vendor);
        this.currCfgNode = createRootConfigNode(context);
    }

    public HuaweiConfigModel getModel() {
        return context.getModel();
    }

    public void walk(IndentedRowConfigNode node) {

        enterFile(node);

        List<IndentedRowConfigNode> children = node.getChildren();
        if (children != null) {
            for (IndentedRowConfigNode child : children) {
                internalWalk(child);
            }
        }

        exitFile(node);
    }

    /**
     * 深度优先遍历AST tree
     * @param node tree node
     */
    private void internalWalk(IndentedRowConfigNode node) {
        enterRow(node);

        List<IndentedRowConfigNode> children = node.getChildren();
        if (children != null) {
            for (IndentedRowConfigNode child : children) {
                internalWalk(child);
            }
        }

        exitRow(node);
    }

    public void enterFile(IndentedRowConfigNode node) {

    }

    public void exitFile(IndentedRowConfigNode node) {

    }

    public void enterRow(IndentedRowConfigNode node) {

        IndentedRowCfgParser.RowContext ctx = node.getCtx();

        HuaweiConfigNode cfgNode = createConfigNode(currCfgNode, ctx);
        switch (cfgNode.getEntityType()) {
            case SYSTEM: {
                sysParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE: {
                intfParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE_IP: {
                ipParser.parse(cfgNode, ctx);
                break;
            }
            case INTERFACE_IP6: {
                ip6Parser.parse(cfgNode, ctx);
                break;
            }
            case BGP: {
                bgpParser.parse(cfgNode, ctx);
                break;
            }
            default: {
                //
            }
        }

        currCfgNode = cfgNode;

    }

    public void exitRow(IndentedRowConfigNode node) {
        if (currCfgNode.getIsEntityRoot()) {
            HuaweiConfigContext context = currCfgNode.getContext();
            HuaweiConfigModel model = context.getModel();
            HuaweiConfigEntityType type = currCfgNode.getEntityType();

            Object entityInstance = context.remParsingEntity(type);
            switch (type) {
                case INTERFACE: {
                    Interface intf = (Interface) entityInstance;
                    model.putIntf(intf.getName(), intf);
                    break;
                }
                case INTERFACE_IP: {
                    Interface intf = (Interface) context.getParsingEntity(INTERFACE);
                    intf.getIps().add((InterfaceIp) entityInstance);
                    break;
                }
                case INTERFACE_IP6: {
                    Interface intf = (Interface) context.getParsingEntity(INTERFACE);
                    intf.getIp6s().add((InterfaceIp6) entityInstance);
                    break;
                }
                case BGP: {
                    Bgp bgp = (Bgp) entityInstance;
                    model.setBgp(bgp);
                    break;
                }
                default: {
                    //
                }
            }
        }

        currCfgNode = currCfgNode.getParent();
    }

    private HuaweiConfigNode createConfigNode(HuaweiConfigNode parent, IndentedRowCfgParser.RowContext ctx) {

        String word0 = ctx.WORD(0) == null ? "" : ctx.WORD(0).getText();
        String word1 = ctx.WORD(1) == null ? "" : ctx.WORD(1).getText();

        HuaweiConfigNode cfgNode = new HuaweiConfigNode();
        cfgNode.setParent(parent);
        cfgNode.setWord0(word0);
        cfgNode.setWord1(word1);
        cfgNode.setIsRoot(false);
        cfgNode.setContext(parent.getContext());

        if ("sysname".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(HuaweiConfigEntityType.SYSTEM);
        } else if ("!Software".equals(word0) && "Version".equals(word1) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(HuaweiConfigEntityType.SYSTEM);
        } else if ("interface".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(HuaweiConfigEntityType.INTERFACE);
        } else if ("bgp".equals(word0) && parent.getIsRoot()) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(HuaweiConfigEntityType.BGP);
        } else if ("ip".equals(word0) && "address".equals(word1)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == HuaweiConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(HuaweiConfigEntityType.INTERFACE_IP);
        } else if ("dual-active".equals(word0) && "backup".equals(word1)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == HuaweiConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(HuaweiConfigEntityType.INTERFACE_IP);
        } else if ("ipv6".equals(word0) && "address".equals(word1)
                && parent.getIsEntityRoot()
                && parent.getEntityType() == HuaweiConfigEntityType.INTERFACE) {
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(HuaweiConfigEntityType.INTERFACE_IP6);
        } else if (parent.getIsRoot()) {
            // 暂不支持解析的一级节点
            cfgNode.setIsEntityRoot(true);
            cfgNode.setEntityType(HuaweiConfigEntityType.UNKNOWN);
        } else {
            // 支持解析或不支持解析的非根节点
            cfgNode.setIsEntityRoot(false);
            // 继承父节点的
            cfgNode.setEntityType(parent.getEntityType());
        }

        return cfgNode;
    }


    private HuaweiConfigNode createRootConfigNode(HuaweiConfigContext context) {
        HuaweiConfigNode cfgNode = new HuaweiConfigNode();
        cfgNode.setParent(null);
        cfgNode.setWord0("root");
        cfgNode.setWord1("root");
        cfgNode.setIsRoot(true);
        cfgNode.setIsEntityRoot(false);
        cfgNode.setEntityType(HuaweiConfigEntityType.ROOT);
        cfgNode.setContext(context);
        return cfgNode;
    }

}
