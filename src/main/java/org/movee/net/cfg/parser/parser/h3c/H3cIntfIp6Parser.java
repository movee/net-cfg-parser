package org.movee.net.cfg.parser.parser.h3c;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.h3c.H3cConfigModel.InterfaceIp6;
import static org.movee.net.cfg.parser.parser.h3c.H3cConfigEntityType.INTERFACE_IP6;


/**
 *
 *
 * @author 
 */
public class H3cIntfIp6Parser {

    public void parse(H3cConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        H3cConfigContext context = configNode.getContext();
        String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();

        if (configNode.getIsEntityRoot()) {
            String[] segs = word2.split("/");
            String ipAddr = segs[0];
            Integer prefixLen = segs.length > 1 ? ParserUtils.parseNullableInteger(segs[1]) : null;
            InterfaceIp6 ip = new InterfaceIp6().setIpAddress(ipAddr).setPrefixLen(prefixLen);

            context.setParsingEntity(INTERFACE_IP6, ip);
        }

    }

}
