package org.movee.net.cfg.parser.parser.xorplus;

import org.movee.net.cfg.parser.utils.ParserUtils;

import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigEntityType.BGP;
import static org.movee.net.cfg.parser.parser.xorplus.XorplusConfigModel.Bgp;

/**
 *
 *
 * @author 
 */
public class XorplusBgpParser {

    public void parse(XorplusConfigNode configNode) {

        XorplusConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {
            Bgp bgp = new Bgp();
            context.setParsingEntity(BGP, bgp);
        } else {
            Bgp bgp = (Bgp) context.getParsingEntity(BGP);

            XorplusConfigNode parent = configNode.getParent();
            if ("bgp-id".equals(word0) && parent.getIsEntityRoot() && parent.getEntityType() == BGP) {
                bgp.setBgpId(word1);
            }

            if ("local-as".equals(word0) && parent.getIsEntityRoot() && parent.getEntityType() == BGP) {
                bgp.setLocalAs(word1);
            }

            if ("local-preference".equals(word0) && parent.getIsEntityRoot() && parent.getEntityType() == BGP) {
                bgp.setLocalPref(ParserUtils.parseNullableLong(word1));
            }

            if ("med".equals(word0) && parent.getIsEntityRoot() && parent.getEntityType() == BGP) {
                bgp.setMed(ParserUtils.parseNullableLong(word1));
            }

            if ("network4".equals(word0) && parent.getIsEntityRoot() && parent.getEntityType() == BGP) {
                bgp.getInet4Prefixes().add(word1);
            }

            if ("network6".equals(word0) && parent.getIsEntityRoot() && parent.getEntityType() == BGP) {
                bgp.getInet6Prefixes().add(word1);
            }
        }

    }

}
