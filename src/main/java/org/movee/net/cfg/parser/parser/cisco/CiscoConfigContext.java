package org.movee.net.cfg.parser.parser.cisco;

import lombok.Data;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 *
 * @author 
 */
@Data
public class CiscoConfigContext {

    private String mgmtIp;
    private String vendor;

    // 解析出来的配置模型
    private CiscoConfigModel model;

    // 当前正在解析的 ConfigModelEntity,  type -> ConfigModelEntity
    private Map<CiscoConfigEntityType, Object> parsingEntities;

    public CiscoConfigContext(String mgmtIp, String vendor) {
        this.mgmtIp = mgmtIp;
        this.vendor = vendor;
        this.model = new CiscoConfigModel();
        this.model.setMgmtIp(mgmtIp);
        this.model.setTimestamp(Instant.now().toEpochMilli());
        this.model.getNetware().setMgmtIp(mgmtIp).setVendor(vendor);
        this.parsingEntities = new LinkedHashMap<>();
    }

    public Object getParsingEntity(CiscoConfigEntityType type) {
        return parsingEntities.get(type);
    }

    public void setParsingEntity(CiscoConfigEntityType type, Object entity) {
        parsingEntities.put(type, entity);
    }

    public Object remParsingEntity(CiscoConfigEntityType type) {
        return parsingEntities.remove(type);
    }
}
