package org.movee.net.cfg.parser.parser.cisco;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

/**
 *
 *
 * @author 
 */
public class CiscoSystemParser {

    public void parse(CiscoConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        CiscoConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        CiscoConfigNode parent = configNode.getParent();
        if (parent.getIsRoot()) {
            CiscoConfigModel.Netware netware = context.getModel().getNetware();
            if ("hostname".equals(word0)) {
                netware.setHostname(word1);
            }

            if ("version".equals(word0)) {
                netware.setSoftwareVersion(word1);
            }
        }

    }

}
