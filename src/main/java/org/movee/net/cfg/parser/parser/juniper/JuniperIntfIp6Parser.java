package org.movee.net.cfg.parser.parser.juniper;
import org.movee.net.cfg.parser.utils.ParserUtils;

/**
 *
 *
 * @author 
 */
public class JuniperIntfIp6Parser {

    public void parse(JuniperConfigNode configNode) {

        JuniperConfigContext context = configNode.getContext();
        String word1 = configNode.getWord1();

        if (configNode.getIsEntityRoot()) {

            String[] segs = word1.split("/");
            String ipAddr = segs[0];
            Integer prefixLen = segs.length > 1 ? ParserUtils.parseNullableInteger(segs[1]) : null;

            JuniperConfigModel.InterfaceIp6 ip = new JuniperConfigModel.InterfaceIp6().setIpAddress(ipAddr).setPrefixLen(prefixLen);
            context.setParsingEntity(JuniperConfigEntityType.INTERFACE_IP6, ip);
        }

    }

}
