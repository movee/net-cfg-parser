package org.movee.net.cfg.parser.api.aspect;

import org.movee.net.cfg.parser.api.v1.ApiNetCfgParserV1;
import org.movee.net.cfg.parser.domain.api.ApiExceptionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 *
 *
 * @author 
 */
@ControllerAdvice(basePackageClasses = {ApiNetCfgParserV1.class})
public class ApiExceptionHandler extends BasicApiExceptionHandler {

    @Autowired
    public ApiExceptionHandler(HttpServletRequest httpRequest) {
        super(httpRequest);
    }

    @Override
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ApiExceptionResponse> handleGenericExcetion(Throwable t) {
        return super.handleGenericExcetion(t);
    }

}
