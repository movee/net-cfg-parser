package org.movee.net.cfg.parser.parser.cisco;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 *
 * @author 
 */
@Getter
@AllArgsConstructor
public enum CiscoConfigEntityType {

    ROOT(""),
    SYSTEM("system"),
    INTERFACE("interface"),
    INTERFACE_IP("ip"),
    INTERFACE_IP6("ipv6"),
    BGP("bgp"),
    BGP_PEER("neighbor"),

    UNKNOWN("unknown");

    private final String key;

}
