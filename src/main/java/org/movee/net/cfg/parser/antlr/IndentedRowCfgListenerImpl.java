package org.movee.net.cfg.parser.antlr;


import org.movee.net.cfg.parser.domain.parser.IndentedRowConfigNode;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;


/**
 *
 *
 * @author 
 */
@Slf4j
public class IndentedRowCfgListenerImpl extends IndentedRowCfgBaseListener {

    private IndentedRowConfigNode currCfgNode = null;
    private IndentedRowConfigNode root = null;
    private final String sectionSplitter;

    public IndentedRowCfgListenerImpl(String sectionSplitter) {
        this.root = createRootConfigNode();
        this.currCfgNode = root;
        this.sectionSplitter = sectionSplitter;
    }

    public IndentedRowConfigNode getParseTree() {
        return root;
    }

    @Override
    public void enterFile(IndentedRowCfgParser.FileContext ctx) {
        super.enterFile(ctx);
    }

    @Override
    public void exitFile(IndentedRowCfgParser.FileContext ctx) {
        super.exitFile(ctx);
    }

    @Override
    public void enterRow(IndentedRowCfgParser.RowContext ctx) {

        if (ctx.WORD().isEmpty()) {
            // 空行
            return;
        }

        String word0 = ctx.WORD(0).getText();
        if (ctx.getStart().getType() == IndentedRowCfgParser.TAB && Objects.equals(sectionSplitter, word0)) {
            // 非顶格注释行
            return;
        }

        if (ctx.getStart().getType() == IndentedRowCfgParser.WORD && Objects.equals(sectionSplitter, word0)) {
            // 顶格注释行
            currCfgNode = root;
            return;
        }

        // 获取偏移
        int offset = 0;
        if (ctx.getStart().getType() == IndentedRowCfgParser.TAB) {
            offset = ctx.TAB(0).getText().length();
        }

        IndentedRowConfigNode cfgNode = createConfigNode(offset, currCfgNode, ctx);
        currCfgNode = cfgNode;
    }

    @Override
    public void exitRow(IndentedRowCfgParser.RowContext ctx) {
        super.exitRow(ctx);
    }

    private IndentedRowConfigNode createConfigNode(int offset,
                                                   IndentedRowConfigNode previous,
                                                   IndentedRowCfgParser.RowContext ctx) {
        IndentedRowConfigNode cfgNode = new IndentedRowConfigNode();
        cfgNode.setOffset(offset);
        cfgNode.setCtx(ctx);

        // 找到父节点
        while (previous != null && offset <= previous.getOffset()) {
            previous = previous.getParent();
        }

        // 维护父子关系
        cfgNode.setParent(previous);
        if (previous != null) {
            previous.appendChild(cfgNode);
        }

        return cfgNode;
    }


    private IndentedRowConfigNode createRootConfigNode() {
        IndentedRowConfigNode cfgNode = new IndentedRowConfigNode();
        cfgNode.setParent(null);
        cfgNode.setOffset(-1);
        cfgNode.setCtx(null);
        return cfgNode;
    }
}
