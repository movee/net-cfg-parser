package org.movee.net.cfg.parser.parser.h3c;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 *
 * @author 
 */
@Getter
@AllArgsConstructor
public enum H3cConfigEntityType {

    ROOT(""),
    SYSTEM("system"),
    INTERFACE("interface"),
    INTERFACE_IP("ip"),
    INTERFACE_IP6("ipv6"),
    BGP("bgp"),

    UNKNOWN("unknown");

    private final String key;

}
