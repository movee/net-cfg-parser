package org.movee.net.cfg.parser.parser.ruijie;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;

/**
 *
 *
 * @author 
 */
public class RuijieSystemParser {

    public void parse(RuijieConfigNode configNode, IndentedRowCfgParser.RowContext ctx) {

        RuijieConfigContext context = configNode.getContext();
        String word0 = configNode.getWord0();
        String word1 = configNode.getWord1();

        RuijieConfigNode parent = configNode.getParent();
        if (parent.getIsRoot()) {
            RuijieConfigModel.Netware netware = context.getModel().getNetware();
            if ("hostname".equals(word0)) {
                netware.setHostname(word1);
            }

            if ("version".equals(word0)) {
                String word2 = ctx.WORD(2) == null ? "" : ctx.WORD(2).getText();

                netware.setSoftwareVersion(String.join(" ", word1, word2));
            }
        }

    }

}
