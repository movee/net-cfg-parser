package org.movee.net.cfg.parser.parser.sonic;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SonicJsonParser {

    private final ObjectMapper jackson = new ObjectMapper();

    public SonicConfigModel parse(String jsonFile) {

        return jackson.convertValue(jsonFile, SonicConfigModel.class);

    }


}
