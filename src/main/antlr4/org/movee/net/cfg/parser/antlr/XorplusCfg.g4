
grammar XorplusCfg ;

// parser

file : NL* (statement NL*)* EOF;
statement : word+ NL* LEFT_BRACE NL* (statement NL*)* RIGHT_BRACE
    | word+ NL;

word : SQUOTE_STRING
    | DQUOTE_STRING
    | WORD;

// lexer
SQUOTE : '\'';
DQUOTE : '"';
LEFT_BRACE : '{';
RIGHT_BRACE : '}';

SQUOTE_STRING : SQUOTE .*? SQUOTE;
DQUOTE_STRING : DQUOTE .*? DQUOTE;
BLOCK_COMMENT : '/*' .*? '*/' -> skip;

SP : [ \t]+ -> skip;
NL : '\r'? '\n';
WORD : ~[ \r\t\n]+;

