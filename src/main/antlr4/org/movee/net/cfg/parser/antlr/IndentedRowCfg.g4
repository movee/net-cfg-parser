
grammar IndentedRowCfg ;

// parser
file : row+;
row : TAB? (WORD TAB?)* NEWLINE;

// lexer
TAB : [ \t]+;
NEWLINE : '\r'? '\n';
WORD : ~[ \r\t\n]+;
