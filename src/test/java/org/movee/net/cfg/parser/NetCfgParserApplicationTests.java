package org.movee.net.cfg.parser;

import org.movee.net.cfg.parser.antlr.IndentedRowCfgLexer;
import org.movee.net.cfg.parser.antlr.IndentedRowCfgParser;
import org.movee.net.cfg.parser.antlr.JuniperCfgLexer;
import org.movee.net.cfg.parser.antlr.JuniperCfgParser;
import org.movee.net.cfg.parser.antlr.XorplusCfgLexer;
import org.movee.net.cfg.parser.antlr.XorplusCfgListenerImpl;
import org.movee.net.cfg.parser.antlr.IndentedRowCfgListenerImpl;
import org.movee.net.cfg.parser.antlr.JuniperCfgListenerImpl;
import org.movee.net.cfg.parser.antlr.XorplusCfgParser;
import org.movee.net.cfg.parser.parser.brocade.BrocadeCfgParseTreeWalker;
import org.movee.net.cfg.parser.parser.h3c.H3cCfgParseTreeWalker;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@Disabled
@AutoConfigureMockMvc
@Slf4j
@SpringBootTest
class NetCfgParserApplicationTests {

    @Autowired
    private MockMvc mvc;


    @Test
    public void testFileParser() throws Exception {

        RequestBuilder rb = MockMvcRequestBuilders.get("/v1/config/file")
                .contentType(MediaType.APPLICATION_JSON)
                .param("mgmtIp", "100.122.194.216");

        mvc.perform(rb)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }


    @Disabled
    @Test
    public void testXorplus() {
        try {
            String fileName = "./src/test/resources/100.122.194.216-test.cfg";
            XorplusCfgLexer lexer = new XorplusCfgLexer(CharStreams.fromFileName(fileName));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            XorplusCfgParser parser = new XorplusCfgParser(tokens);
            ParseTreeWalker walker = new ParseTreeWalker();
            XorplusCfgListenerImpl listener = new XorplusCfgListenerImpl("100.122.194.216", "BAIDU");
            walker.walk(listener, parser.file());

            log.info("==={}", new ObjectMapper().writeValueAsString(listener.getModel()));
        } catch (Exception e) {
            log.info("===={}", ExceptionUtils.getStackTrace(e));
        }
    }

    @Test
    private void testHuawei() {
        try {
            String fileName = "./src/test/resources/100.122.190.43_HUAWEI-test.cfg";
            IndentedRowCfgLexer lexer = new IndentedRowCfgLexer(CharStreams.fromFileName(fileName));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            IndentedRowCfgParser parser = new IndentedRowCfgParser(tokens);
            ParseTreeWalker walker = new ParseTreeWalker();
            IndentedRowCfgListenerImpl listener = new IndentedRowCfgListenerImpl("#");
            walker.walk(listener, parser.file());

            H3cCfgParseTreeWalker huaweiWalk = new H3cCfgParseTreeWalker("100.122.190.43", "HUAWEI");
            huaweiWalk.walk(listener.getParseTree());

        } catch (Exception e) {
            log.info("{}", e.getMessage());
        }
    }

    @Test
    private void testH3c() {
        try {
            String fileName = "./src/test/resources/100.122.190.38_H3C-test.cfg";
            IndentedRowCfgLexer lexer = new IndentedRowCfgLexer(CharStreams.fromFileName(fileName));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            IndentedRowCfgParser parser = new IndentedRowCfgParser(tokens);
            ParseTreeWalker walker = new ParseTreeWalker();
            IndentedRowCfgListenerImpl listener = new IndentedRowCfgListenerImpl("#");
            walker.walk(listener, parser.file());

            H3cCfgParseTreeWalker h3cWalk = new H3cCfgParseTreeWalker("100.122.190.38", "H3C");
            h3cWalk.walk(listener.getParseTree());

        } catch (Exception e) {
            log.info("exception: {}", ExceptionUtils.getStackTrace(e));
        }
    }

    @Test
    private void testBrocade() {
        try {
            String fileName = "./src/test/resources/192.168.180.130_BROCADE-test.cfg";
            IndentedRowCfgLexer lexer = new IndentedRowCfgLexer(CharStreams.fromFileName(fileName));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            IndentedRowCfgParser parser = new IndentedRowCfgParser(tokens);
            ParseTreeWalker walker = new ParseTreeWalker();
            IndentedRowCfgListenerImpl listener = new IndentedRowCfgListenerImpl("!");
            walker.walk(listener, parser.file());

            BrocadeCfgParseTreeWalker brocadeWalk = new BrocadeCfgParseTreeWalker("192.168.180.130", "BROCADE");
            brocadeWalk.walk(listener.getParseTree());

        } catch (Exception e) {
            log.info("{}", e.getMessage());
        }
    }

    @Test
    public void testJuniper() {
        try {
            log.info("=========testJuniper");
            String fileName = "./src/test/resources/192.168.65.180_JUNIPER.cfg";
            JuniperCfgLexer lexer = new JuniperCfgLexer(CharStreams.fromFileName(fileName));
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            JuniperCfgParser parser = new JuniperCfgParser(tokens);
            ParseTreeWalker walker = new ParseTreeWalker();
            JuniperCfgListenerImpl listener = new JuniperCfgListenerImpl("192.168.65.180", "JUNIPER");
            walker.walk(listener, parser.file());

            log.info("testJuniper: {}", new ObjectMapper().writeValueAsString(listener.getModel()));
        } catch (Exception e) {
            log.info("testJuniper: {}", ExceptionUtils.getStackTrace(e));
        }
    }

}
